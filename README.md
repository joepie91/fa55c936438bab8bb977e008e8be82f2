# Video Downloader professional kmdldgcmokdpmacblnehppgkjphcbpnn background.js

__NOTE: This is a fork of the original Gist, with the code made more readable, and additional analysis added.__

This is the source of `background.js` for a now-unpublished Chrome extension called "Video Downloader professional" (ID `kmdldgcmokdpmacblnehppgkjphcbpnn`, since then replaced with another "Video Downloader professional" (ID `bacakpdjpomjaelpkpkabmedhkoongbi`). This script is republished here for educational / research purposes. It has initially been extracted from the extension’s archive available as v2.4 on https://www.crx4chrome.com/.

## Why is this interesting?

The extension has appeared in [malware discussions](https://www.reddit.com/r/chrome/comments/9nza27/psa_video_downloader_professional_chrome/) in the past. Its replacement of [Video downloader professional "bacakpdjpomjaelpkpkabmedhkoongbi"](https://chrome.google.com/webstore/detail/video-downloader-professi/bacakpdjpomjaelpkpkabmedhkoongbi) seems related to the [ownership change of The Great Suspender](https://github.com/greatsuspender/thegreatsuspender/issues/1263#issuecomment-754354645).

## What does the code do?

- Intercepts all requests to:
    - Strip out certain response headers (config.validateFields, string)
    - Strip out the Referer header from every request
    - Optionally add in a fake Referer header based on configuration rules (against the current URL, what page the request originates from, etc.)
- Intercepts all pageloads, to:
    - Report their URLs to the API server (max. once every 2 hours per domain), but only for those domains in a remotely-configured list (/coverage API)
    - Generate a new URL, from an affiliate URL template + the request URL, then:
        - Request that URL and optionally receive a new URL in response
        - Either load that URL in the background, or in a new tab

Possible and likely usecases:
- 'Competitor analytics', a form of industrial espionage, basically tracking how much traffic competitors get, by tracking requests to their sites
- Advertising fraud (through background requests to advertising servers)
- Adware (by opening new tabs with ads)
- Affiliate fraud (through Referer header manipulation/injection)